const bodyTable = document.getElementById("bodyTable");
const Table = document.getElementById("Table");


async function getValueFromServer(){
    let response = await fetch("https://dog.ceo/api/breeds/list/all");
    let data = await response.json();
    console.log(response.status);
    if (response.status == 200){
        setTimeout(()=>{
            const dogs = data.message;
            caricaDati(dogs)
        }, 5000)
    }
}

await getValueFromServer();


function caricaDati(dogs){
    Object.keys(dogs).forEach(breeds => {
        const th = document.createElement("th");
        const td = document.createElement("td");
        const ul = document.createElement("ul");
        const tr = document.createElement("tr");
        const img = document.createElement("td")
        
        th.innerHTML = breeds; 
        
        dogs[breeds].forEach(breed => {
            const li = document.createElement("li");
            li.innerText = breed;
            ul.append(li);
            td.append(ul);
        })

        fetch(`https://dog.ceo/api/breed/${breeds}/images/random/1`)
        .then((response) => response.json())
        .then(data => {
            const photos = data.message
            photos.forEach(photo => {
                img.className = "rounded-xl shadow-xl text-xl bg-white h-[300px] w-[300px]"
                console.log(photo);
                img.style.backgroundImage = `url(${photo})`
                img.style.backgroundRepeat = "no-repeat"
                img.style.backgroundSize = "contain"
            })
        });

        tr.append(th, td, img);
        bodyTable.append(tr);
    });
}

// script select
const select = document.getElementById("breeds");
const container = document.getElementById("container");

fetch("https://dog.ceo/api/breeds/list/all")
.then((response) => response.json())
.then(data => {
const obj = data.message
Object.keys(obj).forEach(key =>{
    if(obj[key].length == 0){
        createOption(key)
    }else{
        obj[key].forEach(element =>{
            createOption(`${element} ${key}`)
        })
    }
})
caricaData(select.value)
})

select.addEventListener("change", (e)=>{
    
    while(container.firstChild && e.target.value != "AllBreeds"){
        container.removeChild(container.firstChild)
        Table.classList.add("d-none")
    }
    if (e.target.value == "AllBreeds"){
        Table.classList.remove("d-none")
    }
    console.log(e.target.value)
    caricaData(e.target.value)
})

      function createOption(value){
        const option = document.createElement("option")
        option.value = value
        option.appendChild(document.createTextNode(value))
        select.appendChild(option)
      }

      function caricaData(breed){
          console.log(breed);
        if(breed == "") return
        if(breed.includes(" ")){
            let newBreed = breed.split(" ");
            console.log(newBreed);
            // newBreed = `${newBreed[1]}-`+`${newBreed[0]}`;
            newBreed = `${newBreed[1]}`;

            console.log(newBreed);
            fetch(`https://dog.ceo/api/breed/${newBreed}/images/random/3`)
            .then(response => response.json())
            .then(data => {
                console.log(data)
                generaCard(data.message)})
        }else{
            fetch(`https://dog.ceo/api/breed/${breed}/images/random/3`)
            .then(response => response.json())
            .then(data => {
                console.log(data)
                generaCard(data.message)
            })
        }
      }

      function generaCard(photos){
        // photos.sort(()=> Math.random() - 0.5)
        photos.forEach(photo => {
            const card = document.createElement("div")
            card.className = "rounded-xl shadow-xl text-xl bg-white h-[200px] w-[200px] bg-cover"
            card.style.backgroundImage = `url(${photo})`
            // card.style.background
            container.appendChild(card)
        })
      }
